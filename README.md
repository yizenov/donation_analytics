
Donation Analytics (Insight Data Engineering)

The project is documented and commented.


Summary: This project is implemented in a stream fashion that is reading input data as it comes. The appraoch is maintaining efficient data structures to keep track of repeated donors,
necessary statistics for a campaign including sorted list to compute a percentile.
Possible failures can be the size of the data structures might exceed the memory withe very large dataset and counting total dollar amount might exceed the size of float(int).


libraries:

1. python 3.5

2. sortedcontainers


running instructions:

1. put the input file into 'input' folder

1. go to the same folder as run.sh file is located

2. sh run.sh on a command line