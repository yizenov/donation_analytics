from src import record_validity
from src import donation_data

import os
import sys

# input_file_name1, input_file_name2, output_file_name = "itcont.txt", "percentile.txt", "repeat_donors.txt"
input_file_name1, input_file_name2, output_file_name = sys.argv[1], sys.argv[2], sys.argv[3]

project_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
opened_input_file = open(project_path + "/input/" + input_file_name2, "r")

percentile = opened_input_file.read()
try:
    percentile = float(percentile)
    percentile /= 100
except ValueError:
    raise ValueError("Percentile value is incorrect.")

opened_input_file.close()
del opened_input_file

opened_output_file = open(project_path + "/output/" + output_file_name, "w")

unique_donor_zipcode_data = set()  # if a new donor_zipcode exist in this set then the donor is a repeat donor.
donation_database = {}

with open(project_path + "/input/" + input_file_name1, 'r') as f:
    for record in f:

        result = record_validity.check_record(record)
        if result is None:
            continue

        donor_zipcode = result.donor + '_' + result.zipcode
        if donor_zipcode not in unique_donor_zipcode_data:
            unique_donor_zipcode_data.add(donor_zipcode)
            continue

        contributions, total, percentile_value = donation_data.print_statistics(donation_database, result, percentile)
        opened_output_file.write('{0}|{1}|{2}|{3}|{4}|{5}\n'.format(result.recipient, result.zipcode, result.year, contributions, total, percentile_value))

        del result

opened_output_file.close()
del opened_output_file

del unique_donor_zipcode_data
del donation_data

print("\nThe end of stream. Finished.\n")
exit()
