
import datetime


class ValidRecord:

    def __init__(self, recipient, donor, amount, zipcode, year):
        self.recipient = recipient
        self.donor = donor
        self.zipcode = zipcode
        self.year = year
        self.amount = amount


def check_date(date_text):
    new_date = None
    try:
        new_date = datetime.datetime.strptime(date_text, '%m%d%Y')
        is_correct = True
    except ValueError:
        is_correct = False

    if is_correct:
        return new_date.year
    return None


def check_amount(amount):
    new_amount = -1
    try:
        new_amount = float(amount)
        is_correct = True
        if new_amount < 1:
            is_correct = False
    except ValueError:
        is_correct = False

    if is_correct:
        return new_amount
    return None


def check_record(record):

    record = record.split('|')

    if record[15]:  # other_id constraints
        return None

    full_name = record[7].split(',')
    if len(full_name) < 2:  # name constraints
        return None  # TODO: is it supposed a person' full name?

    amount = check_amount(record[14])
    if amount is None:  # amount constraints
        return None

    if not record[0]:  # recipient constraints
        return None

    if len(record[10]) < 5:  # zipcode constraints
        return None

    year = check_date(record[13])
    if year is None:  # date constraints
        return None

    return ValidRecord(record[0], full_name[0] + '_' + full_name[1], amount, record[10][:5], year)
