from sortedcontainers import SortedList


def print_statistics(database, record, percentile):

    if record.recipient not in database:
        database[record.recipient] = {}

    recipient_years = database[record.recipient]

    if record.year not in recipient_years:
        recipient_years[record.year] = {}

    recipient_year_zip_codes = recipient_years[record.year]

    if record.zipcode not in recipient_year_zip_codes:
        recipient_year_zip_codes[record.zipcode] = 0, 0, SortedList()  # tuple for number of contributions, total dollar amount, percentile

    statistics = recipient_year_zip_codes[record.zipcode]
    statistics[2].add(record.amount)
    recipient_year_zip_codes[record.zipcode] = (statistics[0] + 1, statistics[1] + record.amount, statistics[2])

    index = len(statistics[2]) * percentile
    if index.is_integer():
        index = int(index)
        percentile_value = (statistics[2][index] + statistics[2][index+1]) / 2  # this can be float number
    else:
        rounded_value = int(round(index))
        percentile_value = statistics[2][rounded_value]

    contributions, total_amount = statistics[0] + 1, statistics[1] + record.amount
    del statistics
    return contributions, total_amount, percentile_value
